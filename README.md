
## Makefile

Build with `./build.sh`.  This allows you to put `mpicc` in your `PATH` *or*
set `CC` and `MPI`.

Be sure to put Turbine in your `PATH`.

On a Cray, build with something like:

~~~~
cd src
export CC=/opt/gcc/4.9.2/bin/gcc
export MPI=/opt/cray/mpt/default/gni/mpich2-gnu/49
export CFLAGS=-I$MPI/include
export LDFLAGS="-L$MPI/lib -lmpich"

PATH=/path/to/turbine/bin:$PATH

./build.sh
~~~~

Put this in a shell script for ease of use.

## Systems

### Cooley

Add to `PATH`:

~~~~
~wozniak/Public/sfw/x86_64/swift-t-launch/stc/bin
~wozniak/Public/sfw/x86_64/swift-t-launch/turbine/bin
~~~~

Set:

~~~~
$ export MODE=cluster QUEUE=default PROJECT=<PICK ONE>
~~~~

Test Swift/T with:

~~~~
$ swift-t -m cobalt hello-world.swift
~~~~

In `mpix_launch_swift`, build with:

~~~~
$ src/build.sh
~~~~

In `mpix_launch_swift/apps/example`, build the example (`main.c`) with:

~~~~
$ ./build-cooley.sh
~~~~

Run with:

~~~~
$ ./run-swift-example-0-cooley.sh
~~~~
