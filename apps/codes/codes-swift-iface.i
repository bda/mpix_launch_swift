
%module codes

typedef int MPI_Comm;

%include <codes-swift-iface.h>

%{
#include <mpi.h>
#include <codes-swift-iface.h>
%}
