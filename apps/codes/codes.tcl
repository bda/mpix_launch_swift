
package require turbine

namespace eval codes {

  # The Swift dataflow function
  proc run_codes { args } {

    set output [ lindex $args 0 ]
    set input  [ lindex $args 1 ]

    if { [ llength $args ] > 2 } {
      set parallelism [ lindex $args 3 ]
      set par_arg "parallelism $parallelism"
    } else {
      set par_arg ""
    }

    # Register a data dependent task
    rule $input "codes::run_codes_body $output $input" \
        type $turbine::WORK {*}$par_arg
  }

  proc run_codes_body { output input } {

    # Get data from Turbine
    set argv [ turbine::retrieve $input ]

    # This is an MPI_Comm
    set comm [ turbine::c::task_comm ]

    # Run CODES
    set exit_code [ run_codes_impl $comm {*}$argv ]

    # Store result in Turbine
    if { [ adlb::rank $comm ] == 0 } {
      turbine::store_integer $output $exit_code
    }
  }

  # Plain Tcl function: no Turbine here
  proc run_codes_impl { comm args } {

    # Convert the Tcl strings to a C char** in argv
    set argc [ turbine::blob_string_list_to_argv "codes-swift" $args argv ]

    # Run CODES
    set exit_code [ run_codes_mpi_replay $comm $argc $argv ]

    # Return Tcl integer
    return $exit_code
  }
}
